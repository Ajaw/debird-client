import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AuthService} from '../service/auth.service';
import {TokenStorage} from '../service/token.storage';

import {HttpClient} from '../../../node_modules/@angular/common/http';
import {DownloadService} from '../service/download.service';
import {$} from 'protractor';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Progress} from '../model/progress';
import {ProgressData} from '../model/progress.data';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css'],
  providers: [AuthService, DownloadService]
})
export class DownloadComponent implements OnInit, OnChanges {

  private _downloadData: BehaviorSubject<ProgressData[]> = new BehaviorSubject([]);
  links: string;
  linkData: any;
  @Input() downloadDataFinished: Progress;


  @Input() downloadData: Progress;

  @Output() myPropChange: EventEmitter<Progress> = new EventEmitter<Progress>();

  constructor(private authSerivce: AuthService, private downloadService: DownloadService) {
  }

  private serverUrl = 'http://localhost:8083/socket';
  private title = 'WebSockets chat';
  private stompClient;


  @Output() progressValueChange = new EventEmitter();

  ngOnInit() {
    this.downloadData = new Progress();
    this.downloadData.data = [];
    this.downloadDataFinished = new Progress();
    this.downloadDataFinished.data = [];
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
      this.stompClient = Stomp.over(ws);
      const that = this;
      const tokenVal = this.authSerivce.getToken();

    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/topic/progress'  , (message) => {

       // console.log(message);
        if (that.downloadData) {
            console.log('not null');
            console.log(that.downloadData);
            const a = JSON.parse(message.body);
            const body = new ProgressData(a.id, a.precentComplete, a.message);
            const hasData = that.downloadData.data.filter(b => b.id === body.id);
            console.log(body.id);
          if ( hasData.length === 0) {
            console.log('added position');
           // that._downloadData.next(that._downloadData.getValue().push(body));
             that.downloadData.data.push(body);
          } else {
            hasData[0].percentComplete  = body.percentComplete;
            hasData[0].message  = body.message;

            // this.progressValueChange.emit();
          }
          console.log(that._downloadData);
          if (that.downloadData) {
            console.log(that.downloadData);
           // that._downloadData.next(that._downloadData.getValue().push());
          }
        } else {
          console.log('creating');
          //this.downloadData = new Progress();
         // this.downloadData.data = [];
          //this.dowloadData.data.push(message.body);
        }
        if (that.downloadData) {
          console.log(that.downloadData.data.filter( b => b.percentComplete > 0));
          that.downloadDataFinished.data = that.downloadDataFinished.data.concat(that.downloadData.data.filter( b => b.percentComplete >= 100));
          console.log(that.downloadDataFinished.data);
          that.downloadData.data = that.downloadData.data.filter( b => b.percentComplete < 100);

        }

      });
      that.stompClient.send('/app/send/message' , {}, tokenVal);
    });

  }

download(link: string): void {
    this.downloadService.downloadLink(this.authSerivce.getToken(), link).subscribe( res =>
    console.log(res));
    this.linkData.linkDTOList = this.linkData.linkDTOList.filter(b => b.link !== link.link);
}

  get curentData() {
    return this._downloadData.asObservable();
  }

 checkLinks(): void {
    const token = this.authSerivce.getToken();
    this.downloadService.getLinksStatus(token, this.links).subscribe(
      res => {
        this.linkData = res;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {


  }

}
