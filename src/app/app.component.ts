import { Component } from '@angular/core';
import {AuthService} from './service/auth.service';
import {Router} from '@angular/router';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {TokenStorage} from './service/token.storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthService, HttpClient, TokenStorage]
})
export class AppComponent {

  constructor(public auth: AuthService, private router: Router) { }

  logout() {
    this.router.navigate(['login']);
  }

}
