
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {User} from '../model/user';
import {TokenStorage} from './token.storage';
import {HttpParams} from '../../../node_modules/@angular/common/http';
@Injectable()
export class ServerStatusService {

  private baseUrl = 'http://localhost:8083' ;
  constructor(private http: HttpClient,  private token: TokenStorage) {


  }





  public get loggedIn(): boolean {
    return (this.token.getToken() !== null);
  }
  public singout() {
    this.token.singOut();
  }

  public getToken(): string {
    return this.token.getToken();
  }

  public getHosterStatus(): Observable<any> {
    console.log(this.baseUrl + '/hoster/status');
    this.http.get(this.baseUrl + '/hoster/status') .subscribe(

      error => console.log(error) // error path
    );
    return this.http.get(this.baseUrl + '/hoster/status', { params : new HttpParams()});
  }



}
