
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {User} from '../model/user';
import {TokenStorage} from './token.storage';
@Injectable()
export class AuthService {

   private baseUrl = 'http://localhost:8083' ;
    constructor(private http: HttpClient,  private token: TokenStorage) {
    }

    attempAuth(username: string, password: string): Observable<any> {
      const user1: User = new User();

      user1.password = password;
      user1.username = username;
      const data = JSON.stringify(user1);
        console.log('attemptAuth ::');
      const postData = new FormData();
      postData.append('user',  data);
        return this.http.post(this.baseUrl + '/logintoapp', postData);
    }



  public get loggedIn(): boolean {
    return (this.token.getToken() !== null);
  }
    public singout() {
      this.token.singOut();
    }

    public getToken(): string {
      return this.token.getToken();
    }



}
