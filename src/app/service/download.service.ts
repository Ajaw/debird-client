
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {User} from '../model/user';
import {TokenStorage} from './token.storage';
import {LinksDto} from '../model/links.dto';
import 'rxjs/add/operator/map';
@Injectable()
export class DownloadService {

  private baseUrl = 'http://localhost:8083';
  constructor(private http: HttpClient,  private token: TokenStorage) {
  }


  public getLinksStatus(token: string, links: string): Observable<any> {
    console.log(this.baseUrl + '/linkcheck');
    this.http.get(this.baseUrl + '/checklinks') .subscribe(

      error => console.log(error) // error path
    );
    return this.http.get(this.baseUrl + '/checklinks', { params : new HttpParams().append('token', token).append('links', links) });
  }

  public downloadLink(token: string, links: string): Observable<any> {
    console.log(this.baseUrl + '/download');
    console.log(links.link);
    const tmpLink  = links.link.toString();
    return this.http.get(this.baseUrl + '/download', { params : new HttpParams().append('token', token).append('links', tmpLink) });
  }



}
