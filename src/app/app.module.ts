import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { SingupComponent } from './singup/singup.component';
import { LoginComponent } from './login/login.component';

import { RouterModule, Routes } from '@angular/router';
import { RouteRoutingModule } from './route/route-routing.module';
import { HomeComponent } from './home/home.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import { MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import { JwtModule } from '@auth0/angular-jwt';
import {AuthService} from './service/auth.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { TokenStorage } from './service/token.storage';
import { DownloadComponent } from './download/download.component';
import { ServerStatusComponent } from './server-status/server-status.component';
import {AuthGuard} from './service/auth.guard';
import {DownloadService} from './service/download.service';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTabsModule} from '@angular/material/tabs';
import { AvalibleHostersComponent } from './avalible-hosters/avalible-hosters.component';
import { AccountStatusComponent } from './account-status/account-status.component';
import {ServerStatusService} from './service/server-status.service';
const routes: Routes = [
  {path : '', component : HomeComponent},
  {path : 'singup', component : SingupComponent},
  {path : 'login', component : LoginComponent},
  {path : 'download', component : DownloadComponent, canActivate: [AuthGuard]}
];
export function jwtOptionsFactory(storage: Storage) {
  return {
    tokenGetter: () => storage.get('jwt_token'),
    whitelistedDomains: ['localhost:8083']
  };
}


@NgModule({
  declarations: [
    AppComponent,
    SingupComponent,
    LoginComponent,
    HomeComponent,
    DownloadComponent,
    ServerStatusComponent,
    AvalibleHostersComponent,
    AccountStatusComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
    BrowserAnimationsModule, MatButtonModule, MatSidenavModule,
    FormsModule, MatGridListModule, MatMenuModule, MatToolbarModule,
    MatIconModule, MatTooltipModule,
    MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule, MatTabsModule,
    RouterModule.forRoot(routes), MatCardModule, FormsModule, MatProgressBarModule
  ],
  providers: [AuthService, HttpClient, AuthGuard, DownloadService, ServerStatusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
