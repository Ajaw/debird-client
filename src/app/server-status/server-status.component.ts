import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';
import {TokenStorage} from '../service/token.storage';
import {DownloadService} from '../service/download.service';
import {ServerStatusService} from '../service/server-status.service';
import {Hoster} from '../model/hoster';
import {Hosters} from '../model/hosters';

@Component({
  selector: 'app-server-status',
  templateUrl: './server-status.component.html',
  styleUrls: ['./server-status.component.css'],
  providers: [ServerStatusService]
})
export class ServerStatusComponent implements OnInit {

  constructor(private router: Router, private serverStatusService: ServerStatusService) { }
data: string;
  hosters: Hosters;

  ngOnInit() {
    this.data = this.getData();
    this.hosters = new Hosters();
    this.hosters.data = [];
  }
s
  getData(): string {
   this.serverStatusService.getHosterStatus().subscribe(
     res => {
       console.log(res);


         const resParse = res;
       for (const tmp in resParse) {
          const obj = resParse[tmp];
         const hoster = new Hoster();
         hoster.active = obj.active;
         hoster.pathToImage = obj.pathToImage;
         this.hosters.data.push(hoster);
       }

     }
   );
   return ' ';
  }

}
