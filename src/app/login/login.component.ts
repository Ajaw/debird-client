import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { TokenStorage } from '../service/token.storage';
import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService, HttpClient, TokenStorage]
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authSerivce: AuthService, private token: TokenStorage) { }

  username: string;
  password: string;

  ngOnInit() {
  }

  login(): void {
    this.authSerivce.attempAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        this.router.navigate(['download']);
      }
    );

  }

}
