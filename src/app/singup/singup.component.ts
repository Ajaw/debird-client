import { Component, OnInit } from '@angular/core';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {FormControl, FormsModule, Validator, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';
import {Router} from '@angular/router';
@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
  password: string;
  username: string;
  email: string;
  shouldRegister: boolean;
  constructor(private router: Router, private http: HttpClient) { }
  emailFormControl: FormControl;
  ngOnInit() {


    this.emailFormControl = new FormControl('', Validators.compose([Validators.required]));

  }



  checkLogIn() {
    const user1: User = new User();
    user1.email = this.email;
    user1.password = this.password;
    user1.username = this.username;
    const data = JSON.stringify(user1);
    const postData = new FormData();
    postData.append('email', this.email);
    console.log(postData);

    this.shouldRegister = false;
    this.http.post('http://localhost:8083/getuser', postData)
      .subscribe(
        res => {
          if (res === false) {
            this.registerUser();
          } else {
            this.router.navigate(['login']);
          }
        }
      );

    console.log(this.email);
    console.log('test');

  }

  registerUser() {
    const user1: User = new User();
    user1.email = this.email;
    user1.password = this.password;
    user1.username = this.username;
    const data = JSON.stringify(user1);
    const postData = new FormData();
    postData.append('user', data);
    console.log(data);
    this.http.post('http://localhost:8083/createuser', postData)
      .subscribe(
        res =>
          console.log(res));
  }


  otherFunciton() {
    console.log('test3');
    console.log('test3');
  }

}

