import {Link} from './link';

export class LinksDto {
  links: Array<Link>;
  fromJSON(obj: any) {
    this.links = obj.links;
    return this;
  }
}
