export class Hoster {
  realId: string;
  hosterAccountList: any[];
  pathToImage: string;
  active: boolean;
  display: boolean;
  totalTransferLeft: number;
  javaClassName?: string;
  name: string;
}
