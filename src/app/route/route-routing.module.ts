import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { SingupComponent } from '../singup/singup.component';

const routes: Routes = [
  {path : '', component : AppComponent},
  {path : 'singup', component : SingupComponent},
  {path : 'login', component : LoginComponent}
];

export const appRouting = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule { }
